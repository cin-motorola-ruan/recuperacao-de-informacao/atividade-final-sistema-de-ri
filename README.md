# Atividade Final - Sistema de RI

Atividade final da disciplina de Recuperação de Informação, Construção de um sistema e RI para indexação e busca de documentos.

Utilização do [Apache Solr](https://lucene.apache.org/solr/) para indexar 120 documentos de 2 temas e realizar consultas.